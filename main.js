console.log("The extension is up and running")

/* 2nd task */

let oldHref = document.location.href;

window.onload = function() {
    let bodyList = document.querySelector("body")

    let observer = new MutationObserver(function(mutations) {
        mutations.forEach(function() {
            if (oldHref !== document.location.href) {
                oldHref = document.location.href;
                if(document.location.href === "https://www.paypal.com/myaccount/transfer/homepage/buy/preview"){
                    let continueButton;
                    let parentDiv;
                    const message = document.createElement('button')
                    message.classList.add("css-1mggxor");
                    message.classList.add("vx_btn");
                    message.classList.add("continue-btn-style");
                    message.setAttribute("data-nemo", "continue")
                    message.style.display = "block"

                    let amount = 800;
                    let inputAmount;

                    message.innerHTML = "Continue";

                    document.body.appendChild(message)
                    document.getElementsByClassName("css-y2xwbt")[0].setAttribute("id", "form-wrapper")
               if(document.getElementById("form-wrapper")){
                   parentDiv = document.getElementsByClassName("css-y2xwbt")[0];
                   continueButton = document.getElementsByClassName("css-y2xwbt")[0].getElementsByTagName("button")[0];
                   parentDiv.insertBefore(message, continueButton)
                   let continueBTNClone = document.getElementsByClassName("continue-btn-style")[0];
                   continueBTNClone.setAttribute('id', 'clone');
                   let continueBTN =  document.getElementsByClassName("css-y2xwbt")[0].getElementsByTagName("button")[1];
                   continueBTN.setAttribute('id', 'original');
                   continueBTN.style.display = "none";

                   continueBTNClone.addEventListener('click', function(e){
                       inputAmount = document.getElementById("fn-amount")?.value;
                       if (parseInt(inputAmount) < amount){
                           e.preventDefault();
                           alert(`Minimum amount of sending money is ${amount}$, please increase your payment's value.`)
                       }
                   });
               }}
            }
        });
    });

    let config = {
        childList: true,
        subtree: true
    };

    observer.observe(bodyList, config);
};

